Narrativa:
Como um usu�rio
desejo informar dois valores de comprimento
de modo que possa saber qual o valor do terceiro comprimento
					 
Cen�rio:  Calculo da hipotenusa
Dado que estou na funcionalidade de c�lculo de tri�ngulos
Quando seleciono o tipo de c�lculo Hipotenusa
E informo 3 para cateto1 
E informo 4 para cateto2
E solicito que o c�lculo seja realizado 
Ent�o a hipotenusa calculada ser� 5
					 
Cen�rio: Calculo do cateto
Dado que estou na funcionalidade de c�lculo de tri�ngulos
Quando seleciono o tipo de c�lculo Cateto
E informo 6 para cateto1 
E informo 10 para hipotenusa
E solicito que o c�lculo seja realizado 
Ent�o o cateto2 calculado ser� 8
