package geometria.teste;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.AfterClass;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;


public class CalculoTriangulo {
private WebDriver driver;
	
	public CalculoTriangulo(WebDriver driver) {
		this.driver = driver;
	}
	
	@AfterClass
	public void close() {
		driver.quit();
	}

	@Given("que estou na funcionalidade de clculo de tringulos")
	public void funcionalidadeCalculo() {
		driver.get(System.getProperty("file:///C:/Users/marce/eclipse-workspace/geometria-2020-1/geometria/src/main/webapp"));
	}

	@When("seleciono o tipo de clculo $campo")
	public void selecionarTipo(String campo) throws InterruptedException {
		driver.findElement(By.id("tipoCalculoSelect")).click();
		Thread.sleep(500);
		driver.findElement(By.id("tipoCalculoSelect")).sendKeys(campo);
		Thread.sleep(500);
		driver.findElement(By.id("tipoCalculoSelect")).sendKeys(Keys.ENTER);
	}

	@When("informo $valor1 para $campo1")
	public void cateto1(String valor,String idcampo) throws InterruptedException {
		driver.findElement(By.id(idcampo)).clear();
		Thread.sleep(500);
		driver.findElement(By.id(idcampo)).sendKeys(valor);
		Thread.sleep(500);
	}
	
	@When("solicito que o clculo seja realizado")
	public void calculo() throws InterruptedException {
		driver.findElement(By.id("calcularBtn")).click();
		Thread.sleep(500);
	}
		
	@Then("o $campo2 calculada ser $resultado")
	public void resultado(String idcampo,String resultado) {
		Assert.assertEquals(driver.findElement(By.id(idcampo)).getAttribute("value"), resultado);
	}

}
