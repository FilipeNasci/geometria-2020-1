package geometria.teste;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class SeleniumFirefoxTeste {
	private static WebDriver driver;
	
	@BeforeAll
	public static void setup(){
	System.setProperty("webdriver.gecko.driver","Driver/geckodriver.exe");
	// System.setProperty("webdriver.firefox.bin", "path/firefox.exe");
	driver = new FirefoxDriver();
	}
	
	@Test
	public void testarHipotenusa() throws InterruptedException {
			driver.get("file:///C:/Users/-Filipe/eclipse-workspace/AvaliacaoTeste/geometria/src/main/webapp/triangulo.html");

			new Select(driver.findElement(By.id("tipoCalculoSelect"))).selectByVisibleText("Hipotenusa");
			
			WebElement pesquisarNoSiteInput = driver.findElement(By.id("cateto1"));
			pesquisarNoSiteInput.sendKeys("3" + Keys.ENTER);
			
			WebElement pesquisarNoSiteInput2 = driver.findElement(By.id("cateto2"));
			pesquisarNoSiteInput2.sendKeys("4" + Keys.ENTER);
			
			WebElement btn = driver.findElement(By.id("calcularBtn"));
			btn.click();
			
			String value = driver.findElement(By.id("hipotenusa")).getAttribute("value");
			Assertions.assertEquals(value, "5");
		}
	
	@Test
	public void testarCateto() throws InterruptedException {
			driver.get("C:\\Users\\-Filipe\\eclipse-workspace\\AvaliacaoTeste\\geometria\\src\\main\\webapp/triangulo.html");

			new Select(driver.findElement(By.id("tipoCalculoSelect"))).selectByVisibleText("Cateto");
			
			WebElement pesquisarNoSiteInput = driver.findElement(By.id("cateto1"));
			pesquisarNoSiteInput.sendKeys("6" + Keys.ENTER);
			
			WebElement pesquisarNoSiteInput2 = driver.findElement(By.id("hipotenusa"));
			pesquisarNoSiteInput2.sendKeys("10" + Keys.ENTER);
			
			WebElement btn = driver.findElement(By.id("calcularBtn"));
			btn.click();
			
			String value = driver.findElement(By.id("cateto2")).getAttribute("value");
			Assertions.assertEquals(value, "8");
		}
	
	@AfterAll
	public static void teardown(){
		driver.quit();
	}
}